# Developement Notes

Install Debian/Ubuntu dependencies for local development.
```
sudo apt-get install --yes git ca-certificates procps wget curl unzip jq
sudo apt-get install --yes python3 python3-venv python3-pip r-base r-cran-tidyverse
```
