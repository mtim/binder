#!/bin/bash
# Run a local dev Jupyter lab envrionment - install/update every time.
# Run from the root of the repository with `bash ./scripts/jupyter-lab.sh`.

# Setup Python Environment
if [ ! -d .venv/ ] ; then
    python3 -m venv .venv
fi
. .venv/bin/activate

# Setup Jupyter Environment
export JUPYTER_CONFIG_DIR=${PWD}/.jupyter
export JUPYTER_DATA_DIR=${PWD}/.local/share/jupyter
install -dv $JUPYTER_CONFIG_DIR $JUPYTER_DATA_DIR

# Setup R Environment
export R_LIBS_USER=.local/share/R
echo "R_LIBS_USER=${R_LIBS_USER}" > .Renviron
install -dv $R_LIBS_USER

# Install Jypter
python3 -m pip install --upgrade pip
python3 -m pip install --upgrade jupyterlab

# Install R Kernel
R --no-save <<EOF
install.packages('IRkernel')
IRkernel::installspec()
EOF

# Run Jupyter Lab
jupyter-lab --port 8080
